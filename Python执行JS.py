'''
方法1: 使用PyExecJS
'''
# import execjs

# js = '''
# function add(x, y) {
#     return x + y;
# }
# '''
# ctx = execjs.compile(js)
# result = ctx.call('add', 1, 2)
# print(result)


"""
方法2: 使用node-vm2
"""
from node_vm2 import VM

with VM() as vm:
   vm.run("""
      var sum = 0, i;
      for (i = 0; i < 10; i++) sum += i;
   """)
   print(vm.run("sum"))