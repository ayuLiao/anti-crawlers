from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options

chrome_options = Options()
# 无头浏览器
chrome_options.add_argument("--headless")

driver = Chrome('chromedriver', options=chrome_options)
driver.get('https://bot.sannysoft.com/')
# 截图保存
driver.save_screenshot('screenshot.png')