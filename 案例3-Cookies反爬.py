import requests

cookies = {
    'session': '.eJwli0sKgCAYBu_yrV3kAzLPEojlTwg-QHMV3j2szTCLmQe9UYV5EJp1PoUMwxliuabdtRNDKkeIBAMuhdiUlmLRYN9og4dZf88uzWjvivQ5KRTGeAHfrR4z.YLRe-w.-tswl5jBYKxiU181bUcW60hzDeM',
    'auth': '872QBRKC424283OBW1622437318',
}

headers = {
    'Connection': 'keep-alive',
    'Pragma': 'no-cache',
    'Cache-Control': 'no-cache',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-User': '?1',
    'Sec-Fetch-Dest': 'document',
    'Referer': 'http://127.0.0.1:8888/cookies',
    'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8',
}

response = requests.get('http://127.0.0.1:8888/movies_cookies', headers=headers, cookies=cookies)
print(response.text)