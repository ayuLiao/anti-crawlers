import requests
import readability

url = 'https://culture.ifeng.com/c/88H83I7afKE'

resp = requests.get(url)
html = resp.content
doc = readability.Document(html)
print('title: ', doc.title())
print('content: ', doc.summary(html_partial=True))
